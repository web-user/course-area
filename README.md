## Local run

#### Create .env file from .env-example and set all data

```sh
$ docker-compose build --no-cache
$ docker-compose up
$ docker-compose run backend python backend/manage.py migrate
$ docker-compose run backend python backend/manage.py makemigrations
$ docker-compose run backend python backend/manage.py createsuperuser
$ docker-compose exec postgres bash
```

### Local urls

```
$ http://0.0.0.0:8000/persons/ - list and create persons (Student/Teacher)
$ http://0.0.0.0:8000/person-detail/<int:pk>/ - Detail Update Destroy
$ http://0.0.0.0:8000/course-list/ - list course, related persons
$ http://0.0.0.0:8000/member-list/ - create member
$ http://0.0.0.0:8000/my-course-list - my members
```