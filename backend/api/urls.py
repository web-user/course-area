from django.urls import include, path
from api import views

# StoreCreateView

urlpatterns = [
    path('persons/', views.PersonList.as_view(), name='persons'),
    path('person-detail/<int:pk>/', views.PersonDetailUpdateDestroy.as_view(), name='person_get'),
    path('member-list/', views.MemberList.as_view(), name='member_list'),
    path('course-list/', views.CourseViewSet.as_view(), name='course_list'),
    path('my-course-list/', views.MyMemberViewSet.as_view({'get': 'list'}), name='my_course_list'),

    path('', views.home, name="home"),
]