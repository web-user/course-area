from rest_framework.generics import ListCreateAPIView, GenericAPIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import mixins
from django.http import JsonResponse

from .serializers import PersonSerializer, CourseSerializer, MemberSerializer, MyMemberSerializer

from apps.person.models import Person
from apps.course.models import Course, Member


class PersonList(ListCreateAPIView):
    """
    View is used for create Person
    """
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class PersonDetailUpdateDestroy(mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    GenericAPIView):
    """
    View is used for get / update / delete Student
    """
    queryset = Person.objects.all()
    serializer_class = PersonSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)



class CourseViewSet(ListCreateAPIView):
    """
    A simple ViewSet for listing store.
    """
    queryset = Course.objects.filter(status='available')
    serializer_class = CourseSerializer


class MemberList(ListCreateAPIView):
    """
    View is used for create Student
    """

    queryset = Member.objects.all()
    serializer_class = MemberSerializer



class MyMemberViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing store.
    """

    def list(self, request):
        person = request.user
        print(person.id)
        print("-------========= 11111111eeCUUUUUUUe44ee223333")
        queryset = Member.objects.filter(person=person)
        print("-------========= 1111dd1111CUUUUUUU223ee333")
        print([x.course_id for x in queryset])
        # queryset = Course.objects.filter(id__in=[x.course_id for x in queryset])
        serializer = MyMemberSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)



def home(request):
    return JsonResponse(data={"message": "success"}, status=200)
