from apps.person.models import Person
from apps.course.models import Course, Member
from rest_framework import serializers


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(
        style={'input_type': 'password'},
        write_only=True
    )

    class Meta:
        model = Person
        fields = ['id', 'name', 'role', 'phone_number', 'password']


class CourseSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Course
        fields = ['id', 'title', 'persons', 'status']


class DatailCourseSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Course
        fields = ['id', 'title', 'status']


class MemberSerializer(serializers.ModelSerializer):

    person = serializers.PrimaryKeyRelatedField(queryset=Person.objects.all(), default=serializers.CurrentUserDefault())
    course = serializers.PrimaryKeyRelatedField(queryset=Course.objects.filter(status='available'))

    # Use this method for the custom field


    class Meta:
        model = Member
        fields = ['person', 'course']

#

class MyMemberSerializer(serializers.ModelSerializer):

    person = serializers.PrimaryKeyRelatedField(queryset=Person.objects.all(), default=serializers.CurrentUserDefault())
    course = DatailCourseSerializer()

    # Use this method for the custom field


    class Meta:
        model = Member
        fields = ['person', 'course']

