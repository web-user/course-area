from django.db import models
from django.urls import reverse

from apps.person.models import Person

class Course(models.Model):

    STATUS_COURSE = [
        ('available', 'Available'),
        ('notavailable', 'Notavailable'),
    ]

    title = models.CharField(
        verbose_name="Title Course",
        max_length=300)

    status = models.CharField(
        verbose_name="Status Course",
        default='available',
        choices=STATUS_COURSE,
        max_length=100)

    persons = models.ManyToManyField(
        Person,
        verbose_name="Persons",
        through='Member')

    def get_absolute_url(self):
        return reverse('course_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Course"
        verbose_name_plural = "Course"


class Member(models.Model):
    person = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        verbose_name="Member Person",
        related_name="user_student")

    course = models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
        verbose_name="Course",
        related_name="course_member")