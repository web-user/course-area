from django.contrib.auth.models import AbstractBaseUser
from django.db import models

from .manager import PersonManager
from apps.validations import PHONE_NUMBER_REGEX


class Person(AbstractBaseUser):
    """
    User Person from PersonManager
    """

    PERSON_ROLES_CHOICES = [
        ('student', 'Student'),
        ('teacher', 'Teacher'),
    ]

    name = models.CharField(
        verbose_name="Name Person",
        max_length=200)

    role = models.CharField(
        max_length=50,
        choices=PERSON_ROLES_CHOICES,
        default='student',
    )

    phone_number = models.CharField(
        verbose_name="Phone Number",
        max_length=15,
        unique=True,
        validators=[PHONE_NUMBER_REGEX])

    admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'phone_number'

    # Settings
    objects = PersonManager()

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        return True

    @property
    def is_admin(self):
        return self.admin


    def __str__(self):
        return f'{self.name} - Role: {self.role}'

    class Meta:
        verbose_name = "User - Person"
        verbose_name_plural = "User - Person"
