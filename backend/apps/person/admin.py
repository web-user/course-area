from django.contrib import admin

from .models import Person



@admin.register(Person)
class AdminUser(admin.ModelAdmin):
	fields = ('name', 'rol_person', 'phone_number', 'password',)

